//  Directions
const uint8_t TOP = 1;
const uint8_t RIGHT = 2;
const uint8_t DOWN = 3;
const uint8_t LEFT = 4;

//  Field
const uint8_t FIELD_WIDTH = 11;
const uint8_t FIELD_HEIGHT = 10;

//  Snake 
const uint8_t MAX_SNAKE_LENGTH = FIELD_WIDTH*FIELD_HEIGHT;
uint32_t snakeColor = pixels.Color(127,127,127);  //  Snake is white
uint32_t fruitColor = pixels.Color(255,102,51);   //  Fruit is orange

//  Variables
uint8_t snakeX[MAX_SNAKE_LENGTH];
uint8_t snakeY[MAX_SNAKE_LENGTH];
uint8_t snakeLength = 1;
uint8_t fruitX,fruitY;
unsigned long prevTime = 0;                        // for gamedelay (ms)
unsigned long delayTime = 500;
unsigned long prevFruit = 0;
unsigned long fruitDelayTime = 200;
bool fruitOn = true;

void setupSnake() {
  randomSeed(analogRead(0));
  snakeX[0] = floor(FIELD_WIDTH/2);
  snakeY[0] = floor(FIELD_HEIGHT/2);
  prevTime = millis();
  prevFruit = millis();
  makeFruit();
  snakeLength = 1;
}

void loopSnake() {
//  Serial.println("loop.snake");
//  Serial.print("Prev: ");
//  Serial.print(prevTime);
//  Serial.print(" Now: ");
//  Serial.println(millis());
  if ((prevTime + delayTime) < millis()) {
    nextStep();
    prevTime = millis();
  }
  draw();
}

boolean inPlayField(int x, int y){
  return (x>=0) && (x<FIELD_WIDTH) && (y>=0) && (y<FIELD_HEIGHT);
}

void nextStep() {
  for (int i=snakeLength-1;i>0;i--) {
    snakeX[i] = snakeX[i-1];
    snakeY[i] = snakeY[i-1];  
  }
  switch(snakeDirection){
    case TOP:
      snakeY[0] = snakeY[0]-1;
      break;
    case RIGHT:
      snakeX[0] = snakeX[0]+1;
      break;
    case DOWN:
      snakeY[0] = snakeY[0]+1;
      break;
    case LEFT:
      snakeX[0]=snakeX[0]-1;
      break;
  }
  if (!inPlayField(snakeX[0],snakeY[0]) || isPartOfSnakeButNotItsHead(snakeX[0],snakeY[0])) {
    //  Game over
    upToDown(fruitColor,50);
    setupSnake();
    return;
  }
  if((snakeX[0] == fruitX) && (snakeY[0] == fruitY)){
    snakeLength++;
    snakeX[snakeLength-1] = snakeX[snakeLength-2];
    snakeY[snakeLength-1] = snakeY[snakeLength-2];
    if(snakeLength < MAX_SNAKE_LENGTH){      
      makeFruit();
    } else {
      fruitX = fruitY = -1;
    }
  }
}

void makeFruit(){
  randomSeed(analogRead(0));
  int x, y;
  x = random(0, FIELD_WIDTH);
  y = random(0, FIELD_HEIGHT);
  while(isPartOfSnake(x, y)){
    x = random(0, FIELD_WIDTH);
    y = random(0, FIELD_HEIGHT);
  }
  fruitX = x;
  fruitY = y;
  Serial.print("Made fruit at: ");
  Serial.print(fruitX);
  Serial.print(" ");
  Serial.println(fruitY);
}

boolean isPartOfSnake(int x, int y) {
  for (int i=0;i<snakeLength;i++) {
    if (snakeX[i] == x && snakeY[i] == y) {
      return true;
    }
  }
  return false;
}

boolean isPartOfSnakeButNotItsHead(int x, int y) {
  for (int i=1;i<snakeLength;i++) {
    if (snakeX[i] == x && snakeY[i] == y) {
      return true;
    }
  }
  return false;
}

void draw() {
//  Serial.println("Draw");
  pixels.clear();
  drawSnake();
  if ((prevFruit + fruitDelayTime) < millis()) {
    fruitOn = !fruitOn;
    prevFruit = millis();
  }
  if (fruitOn) {
    drawFruit();
  }
  pixels.show();
}

void drawSnake() {
  for (int i=0;i<snakeLength;i++) {
    pixels.setPixelColor(pixelForXY(snakeX[i],snakeY[i]),Wheel(i*2));
  }
}

void drawFruit() {
  pixels.setPixelColor(pixelForXY(fruitX,fruitY),fruitColor);
}

/*
 * 0,0 = 0;
 * 1,0 = 1;
 * 0,1 = 21;
 * 3,5 = 
 */
uint8_t pixelForXY(uint8_t x,uint8_t y) {
  uint8_t pixel;
  if (y%2==0) {
    pixel = y*FIELD_WIDTH+x;
  } else {
    pixel = ((y+1)*11)-(x+1);
  }
  return pixel;
}

