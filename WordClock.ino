#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <TimeLib.h>
#include <DS1302RTC.h>
#include <Adafruit_NeoPixel.h>

// Set RTC: CE, IO, CLK
DS1302RTC RTC(16,5,4);
const int TIMEZONE = 2; //  2 = Amsterdam (1 = Amsterdam wintertijd)

//  NeoPixels
#define PIXELPIN 2 // D4
#define NUMPIXELS 114 //  Total number of pixels
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIXELPIN, NEO_GRB + NEO_KHZ800);
uint32_t c = pixels.Color(127,127,127);

const uint8_t COLUMNS = 11;
const uint8_t ROWS = 10;
uint8_t PREV_HOUR = 0;
uint8_t PREV_MINUTE = 0;
uint16_t WHEEL_COUNT = 0;

bool isPlayingSnake = false;
uint8_t snakeDirection = 1;
bool isTurnedOn = true;

//  WIFI
char ssid[] = "!Jort";  //  your network SSID (name)
char pass[] = "appelboom";       // your network password
unsigned int localPort = 2390;      // local port to listen for UDP packets
IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "time.nist.gov";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP udp;
ESP8266WebServer server(80);

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // START NTP SERVER
  Serial.println("Starting UDP");
  udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(udp.localPort());

  if (RTC.haltRTC()) {
    Serial.print("The DS1302 is stopped.  Please set time");
    Serial.println("to initialize the time and begin running.");
  }
  if (!RTC.writeEN()) {
    Serial.println("The DS1302 is write protected. This normal.");
  }

  time_t t = getTime();

  Serial.print("Time:");
  Serial.println(t);

  if(RTC.set(t) == 0) {
    Serial.println("RTC Set!");
  } else {
    Serial.println("Failed to set RTC.");
  }

  udp.stop();
  //  End NTP SERVER

  server.on("/",handle_msg);
  server.begin();

  //  Start NeoPixels
  pixels.begin();
  pixels.setBrightness(64);
}

void loop()
{
  server.handleClient();
  if (isPlayingSnake) {
    loopSnake();
  } else if (isTurnedOn) {
    displayTime();
  }
}

void handle_msg()
{
  server.send(200,"text/html","{success:true}");

  // POWER = 1 ON POWER = 2 OFF
  String powerString = server.arg("power");
  int power = powerString.toInt();
  if (power == 1) {
      Serial.println("Turn on");
      isTurnedOn = true;
      forceDisplayTime();
  } else if (power == 2) {
      Serial.println("Turn off: "+power);
      isTurnedOn = false;
      pixels.clear();
      pixels.show();
  }

  String snakeString = server.arg("snake");
  int snake = snakeString.toInt();
  if (snake == 1) {
    isPlayingSnake = true;
    setupSnake();
    Serial.println("Start snake");
  } else if (snake == 2) {
    isPlayingSnake = false;
    Serial.println("Stop snake");
    forceDisplayTime();
  }

  String snakeDirectionStr = server.arg("direction");
  int snakeDirectionInt = snakeDirectionStr.toInt();
  if (isPlayingSnake == true && snakeDirectionInt > 0 && snakeDirectionInt < 5) {
    snakeDirection = snakeDirectionInt;
  }

  String brightnessString = server.arg("brightness");
  int brightness = brightnessString.toInt();
  
  if (brightness > 0 && brightness <= 255) {
    Serial.print("Set brightness: ");
    Serial.println(brightness);
    pixels.setBrightness(brightness);
    pixels.show();
  }

  String colorRs = server.arg("r");
  String colorGs = server.arg("g");
  String colorBs = server.arg("b");
  uint8_t colorR = colorRs.toInt();
  uint8_t colorG = colorGs.toInt();
  uint8_t colorB = colorBs.toInt();

  if (colorR || colorG || colorB) {
    Serial.print("Set color: RGB(");
    Serial.print(colorR);
    Serial.print(",");
    Serial.print(colorG);
    Serial.print(",");
    Serial.print(colorB);
    Serial.println(")");
    
    c = pixels.Color(colorR,colorG,colorB);
    forceDisplayTime();
  }
}

void displayTime()
{
  time_t t = RTC.get();
  //printTime(t);

  uint8_t HOUR = hour(t);
  uint8_t MINUTE = minute(t);

  if (HOUR != PREV_HOUR || MINUTE != PREV_MINUTE) {
    PREV_HOUR = HOUR;
    PREV_MINUTE = MINUTE;

    displayTime(HOUR,MINUTE);
  }
}

void forceDisplayTime()
{
    time_t t = RTC.get();
  //printTime(t);

  uint8_t HOUR = hour(t);
  uint8_t MINUTE = minute(t);

  PREV_HOUR = HOUR;
  PREV_MINUTE = MINUTE;

  displayTime(HOUR,MINUTE);
}

unsigned long getTime()
{
  unsigned long result;
  
  //get a random server from the pool
  WiFi.hostByName(ntpServerName, timeServerIP); 

  sendNTPpacket(timeServerIP); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  
  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("no packet yet");
  }
  else {
    Serial.print("packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = " );
    Serial.println(secsSince1900);

    // now convert NTP time into everyday time:
    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
    Serial.println(epoch);


    // print the hour, minute and second:
    Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
    Serial.print((epoch  % 86400L) / 3600); // print the hour (86400 equals secs per day)
    Serial.print(':');
    if ( ((epoch % 3600) / 60) < 10 ) {
      // In the first 10 minutes of each hour, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.print((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
    Serial.print(':');
    if ( (epoch % 60) < 10 ) {
      // In the first 10 seconds of each minute, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.println(epoch % 60); // print the second

    epoch = epoch + 60*60*TIMEZONE;
    // print the hour, minute and second:
    Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
    Serial.print((epoch  % 86400L) / 3600); // print the hour (86400 equals secs per day)
    Serial.print(':');
    if ( ((epoch % 3600) / 60) < 10 ) {
      // In the first 10 minutes of each hour, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.print((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
    Serial.print(':');
    if ( (epoch % 60) < 10 ) {
      // In the first 10 seconds of each minute, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.println(epoch % 60); // print the second

    result = epoch;
  }
  // wait ten seconds before asking for the time again
  return result;
}

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(IPAddress& address)
{
  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

void printTime(time_t t)
{
    Serial.print(hour(t));
    Serial.print(":");
    Serial.print(minute(t));
    Serial.print(":");
    Serial.println(second(t));
}

void upToDown(uint32_t color, uint8_t wait)
{
  for (int r=0;r<ROWS;r++) {
    pixels.clear();
    for (int c=0;c<COLUMNS;c++) {
      pixels.setPixelColor((r*COLUMNS)+c,color);
    }
    pixels.show();
    delay(wait);
  }
  pixels.clear();
}

//void leftToRight(uint32_t color, uint8_t wait)
//{
//  for (int c=0;c<COLUMNS;c++) {
//    pixels.clear();
//    for (int r=0;r<ROWS;r++) {
//
//      //c/COLUMNS
//
//      
//      uint8_t pixel = (r*COLUMS);
//      
//      0,21,22,
//      pixels.setPixelColor((c*ROWS)+r,color);
//    }
//    pixels.show();
//    delay(wait);
//  }
//  pixels.clear();
//}

void displayTime(uint8_t uur, uint8_t minuut)
{
  pixels.clear();
  if (minuut >= 20) {
    uur = uur+1;
  }
  if (uur >= 12) {
    uur = uur-12;
  }
  
  if (uur == 0) {
    twaalf(c);
  } else if (uur == 1) {
    een(c);
  } else if (uur == 2) {
    twee(c);
  } else if (uur == 3) {
    drie(c);
  } else if (uur == 4) {
    vier(c);
  } else if (uur == 5) {
    vijf2(c);
  } else if (uur == 6) {
    zes(c);
  } else if (uur == 7) {
    zeven(c);
  } else if (uur == 8) {
    acht(c);
  } else if (uur == 9) {
    negen(c);
  } else if (uur == 10) {
    tien2(c);
  } else if (uur == 11) {
    elf(c);
  } else if (uur == 12) {
    twaalf(c);
  }
  
  het(c);
  is(c);
  if (minuut >= 0 && minuut < 5) {
    heelUur(c);
  } else if (minuut >= 5 && minuut < 10) {
    vijf1(c);over1(c);
  } else if (minuut >= 10 && minuut < 15) {
    tien1(c);over2(c);
  } else if (minuut >= 15 && minuut < 20) {
    kwart(c);over2(c);
  } else if (minuut >= 20 && minuut < 25) {
    tien1(c);voor1(c);half(c);
  } else if (minuut >= 25 && minuut < 30) {
    vijf1(c);voor1(c);half(c);
  } else if (minuut >= 30 && minuut < 35) {
    half(c);
  } else if (minuut >= 35 && minuut < 40) {
    vijf1(c);over1(c);half(c);
  } else if (minuut >= 40 && minuut < 45) {
    tien1(c);over1(c);half(c);
  } else if (minuut >= 45 && minuut < 50) {
    kwart(c);voor2(c);
  } else if (minuut >= 50 && minuut < 55) {
    tien1(c);voor1(c);
  } else if (minuut >= 55 && minuut < 60) {
    vijf1(c);voor1(c);
  }

  if (minuut % 5 == 1) {
    minuut1(c);
  } else if (minuut % 5 == 2) {
    minuut1(c);minuut2(c);
  } else if (minuut % 5 == 3) {
    minuut1(c);minuut2(c);minuut3(c);
  } else if (minuut % 5 == 4) {
    minuut1(c);minuut2(c);minuut3(c);minuut4(c);
  }
  
  pixels.show();
}

void het(uint32_t c)
{
  pixels.setPixelColor(0,c);
  pixels.setPixelColor(1,c);
  pixels.setPixelColor(2,c);
}

void is(uint32_t c)
{
  pixels.setPixelColor(4,c);
  pixels.setPixelColor(5,c);
}

void vijf1(uint32_t c)
{
  pixels.setPixelColor(7,c);
  pixels.setPixelColor(8,c);
  pixels.setPixelColor(9,c);
  pixels.setPixelColor(10,c);
}

void tien1(uint32_t c)
{
  pixels.setPixelColor(18,c);
  pixels.setPixelColor(19,c);
  pixels.setPixelColor(20,c);
  pixels.setPixelColor(21,c);
}

void voor1(uint32_t c)
{
  pixels.setPixelColor(11,c);
  pixels.setPixelColor(12,c);
  pixels.setPixelColor(13,c);
  pixels.setPixelColor(14,c);
}

void over1(uint32_t c)
{
  pixels.setPixelColor(22,c);
  pixels.setPixelColor(23,c);
  pixels.setPixelColor(24,c);
  pixels.setPixelColor(25,c);
}

void kwart(uint32_t c)
{
  pixels.setPixelColor(28,c);
  pixels.setPixelColor(29,c);
  pixels.setPixelColor(30,c);
  pixels.setPixelColor(31,c);
  pixels.setPixelColor(32,c);
}

void half(uint32_t c)
{
  pixels.setPixelColor(40,c);
  pixels.setPixelColor(41,c);
  pixels.setPixelColor(42,c);
  pixels.setPixelColor(43,c);
}

void over2(uint32_t c)
{
  pixels.setPixelColor(33,c);
  pixels.setPixelColor(34,c);
  pixels.setPixelColor(35,c);
  pixels.setPixelColor(36,c);
}

void voor2(uint32_t c)
{
  pixels.setPixelColor(44,c);
  pixels.setPixelColor(45,c);
  pixels.setPixelColor(46,c);
  pixels.setPixelColor(47,c);
}

void een(uint32_t c)
{
  pixels.setPixelColor(51,c);
  pixels.setPixelColor(52,c);
  pixels.setPixelColor(53,c);
}

void twee(uint32_t c)
{
  pixels.setPixelColor(62,c);
  pixels.setPixelColor(63,c);
  pixels.setPixelColor(64,c);
  pixels.setPixelColor(65,c);
}

void drie(uint32_t c)
{
  pixels.setPixelColor(55,c);
  pixels.setPixelColor(56,c);
  pixels.setPixelColor(57,c);
  pixels.setPixelColor(58,c);
}

void vier(uint32_t c)
{
  pixels.setPixelColor(66,c);
  pixels.setPixelColor(67,c);
  pixels.setPixelColor(68,c);
  pixels.setPixelColor(69,c);
}

void vijf2(uint32_t c)
{
  pixels.setPixelColor(70,c);
  pixels.setPixelColor(71,c);
  pixels.setPixelColor(72,c);
  pixels.setPixelColor(73,c);
}

void zes(uint32_t c)
{
  pixels.setPixelColor(74,c);
  pixels.setPixelColor(75,c);
  pixels.setPixelColor(76,c);
}

void zeven(uint32_t c)
{
  pixels.setPixelColor(83,c);
  pixels.setPixelColor(84,c);
  pixels.setPixelColor(85,c);
  pixels.setPixelColor(86,c);
  pixels.setPixelColor(87,c);
}

void negen(uint32_t c)
{
  pixels.setPixelColor(77,c);
  pixels.setPixelColor(78,c);
  pixels.setPixelColor(79,c);
  pixels.setPixelColor(80,c);
  pixels.setPixelColor(81,c);
}

void acht(uint32_t c)
{
  pixels.setPixelColor(88,c);
  pixels.setPixelColor(89,c);
  pixels.setPixelColor(90,c);
  pixels.setPixelColor(91,c);
}

void tien2(uint32_t c)
{
  pixels.setPixelColor(92,c);
  pixels.setPixelColor(93,c);
  pixels.setPixelColor(94,c);
  pixels.setPixelColor(95,c);
}

void elf(uint32_t c)
{
  pixels.setPixelColor(96,c);
  pixels.setPixelColor(97,c);
  pixels.setPixelColor(98,c);
}

void twaalf(uint32_t c)
{
  pixels.setPixelColor(104,c);
  pixels.setPixelColor(105,c);
  pixels.setPixelColor(106,c);
  pixels.setPixelColor(107,c);
  pixels.setPixelColor(108,c);
  pixels.setPixelColor(109,c);
}

void heelUur(uint32_t c)
{
  pixels.setPixelColor(99,c);
  pixels.setPixelColor(100,c);
  pixels.setPixelColor(101,c);
}

void minuut1(uint32_t c)
{
  pixels.setPixelColor(113,c);
}

void minuut2(uint32_t c)
{
  pixels.setPixelColor(112,c);
}

void minuut3(uint32_t c)
{
  pixels.setPixelColor(111,c);
}

void minuut4(uint32_t c)
{
  pixels.setPixelColor(110,c);
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
